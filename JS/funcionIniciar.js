import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-auth.js';


const firebaseConfig = {
    apiKey: "AIzaSyBhwFVMJNoGAQ-fgFkkncZsMW5f-C6j6oI",
    authDomain: "administradorweb-db62c.firebaseapp.com",
    databaseURL: "https://administradorweb-db62c-default-rtdb.firebaseio.com",
    projectId: "administradorweb-db62c",
    storageBucket: "administradorweb-db62c.appspot.com",
    messagingSenderId: "421982072588",
    appId: "1:421982072588:web:6605dd0096fe95bcdce329"
  };
  
const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);

    
const correo = document.getElementById("correo");
const contrasena = document.getElementById("contrasena");
const errorMensaje = document.getElementById("errorMensaje");

const boton = document.getElementById("boton");
boton.addEventListener("click", iniciarSesion);
  
  function iniciarSesion() {
    const correo = document.getElementById("correo").value;
    const contrasena = document.getElementById("contrasena").value;
  
    signInWithEmailAndPassword(auth, correo, contrasena)
      .then((userCredential) => {
        window.location.href = "/HTML/administrador.html";
      })
      .catch((error) => {
        console.log("Hubo un error", error);
      });
  }