import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {
    apiKey: "AIzaSyBhwFVMJNoGAQ-fgFkkncZsMW5f-C6j6oI",
    authDomain: "administradorweb-db62c.firebaseapp.com",
    databaseURL: "https://administradorweb-db62c-default-rtdb.firebaseio.com",
    projectId: "administradorweb-db62c",
    storageBucket: "administradorweb-db62c.appspot.com",
    messagingSenderId: "421982072588",
    appId: "1:421982072588:web:6605dd0096fe95bcdce329"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

window.addEventListener('DOMContentLoaded', (event) => {
  tablaproductos();
  mostrar();
});


var codigo = 0;
var nombre = "";
var precio = "";
var url = "";
var agregarr = document.getElementById('agregar');
var buscarr = document.getElementById('buscar');
var actualizarr = document.getElementById('actualizar');
var borrarr = document.getElementById('borrar');

borrarr.addEventListener('click', eliminar);
agregarr.addEventListener('click', insertar);
actualizarr.addEventListener('click', actualizar);
buscarr.addEventListener('click', buscar);


function leerInputs() {
  codigo = document.getElementById('codigo').value;
  nombre = document.getElementById('nombre').value;
  precio = document.getElementById('precio').value;
  url = document.getElementById('url').value;
}

function insertar() {

  leerInputs();
  if (codigo === "" || nombre === "" || precio === "" || url === "") {
    alert("No agrego toda la informacion");
    return;
  }

  set(refS(db, 'joyas/' + codigo), {
    nombre: nombre,
    precio: precio,
    url: url
  }).then(() => {
    alert("Producto Agregado");
    limpiar();
    tablaproductos();   
  }).catch((error) => {
    alert("Hubo un error" + error);
  });
}

function buscar() {
    leerInputs();
  if (codigo === "") {
    alert("No ingreso un id");
    return;
  }
  const dbref = refS(db);

  get(child(dbref, 'joyas/' + codigo)).then((snapshot) => {
    if (snapshot.exists()) {
      nombre = snapshot.val().nombre;
      precio = snapshot.val().precio;
      url = snapshot.val().url;
      escribir();
    } else {
      alert("No se ingreso un id existente");
    }
  });
}

function actualizar() {
  leerInputs();
  if (codigo === "" || nombre === "" || precio === "" || url === "") {
    alert("No agrego toda la informacion");
    return;
  }

  update(refS(db, 'joyas/' + codigo), {
    nombre: nombre,
    precio: precio,
    url: url
  }).then(() => {
    alert("Producto actualizado");
    limpiar();
    tablaproductos();   
  }).catch((error) => {
    alert("Hubo un error" + error);
  });
}


function eliminar() {

    leerInputs();
  if (codigo === "") {
    alert("No ingreso un id");
    return;
  }

  remove(refS(db, 'joyas/' + codigo))
    .then(() => {
        alert("Producto borrado");
        limpiar();
        tablaproductos();   
      }).catch((error) => {
        alert("Hubo un error" + error);
      });
}


function limpiar() {
  document.getElementById('codigo').value = '';
  document.getElementById('nombre').value = '';
  document.getElementById('precio').value = '';
  document.getElementById('url').value = '';
}


function escribir() {
    document.getElementById('nombre').value = nombre;
    document.getElementById('precio').value = precio;
    document.getElementById('url').value = url;
  }

  
function tablaproductos() {
    const dbRef = refS(db, 'joyas');
    const tabla = document.getElementById('tablaproductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
  
    onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const data = childSnapshot.val();
  
        var fila = document.createElement('tr');
  
        var celdacodigo = document.createElement('td');
        celdacodigo.textContent = childKey;
        fila.appendChild(celdacodigo);
  
        var celdaNombre = document.createElement('td');
        celdaNombre.textContent = data.nombre;
        fila.appendChild(celdaNombre);
  
        var celdaPrecio = document.createElement('td');
        celdaPrecio.textContent = "$" + data.precio;
        fila.appendChild(celdaPrecio);
  
        tbody.appendChild(fila);
      });
    }, { onlyOnce: true });
  }


