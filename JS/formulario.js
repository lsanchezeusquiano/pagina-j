
const loginButton = document.querySelector('.login-button');
const modal = document.getElementById('login-form');
const closeButton = document.getElementById('close-button');

function hideModal() {
  modal.style.display = 'none';
}


hideModal();

if (loginButton && modal && closeButton) {
  loginButton.addEventListener('click', function() {
    modal.style.display = 'block';
  });

  closeButton.addEventListener('click', function() {
    modal.style.display = 'none';
  });
}
